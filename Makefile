install: vendor
	@mkdir -p tests/units
	@mkdir -p tests/phpunit
	@mkdir -p src

phpunit:
	@./vendor/bin/phpunit -c phpunit.xml

atoum:
	@./vendor/bin/atoum --autoloop -d tests/units --debug

vendor: composer.phar
	@./composer.phar install

composer.phar:
	@curl -sS https://getcomposer.org/installer | php > /dev/null 2>&1;

clean:
	@rm -rf vendor composer.phar

