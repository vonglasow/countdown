<?php
use \mageekguy\atoum;

$images = __DIR__.'/vendor/atoum/atoum/resources/images/logo';
$notifier = new \mageekguy\atoum\report\fields\runner\result\notifier\image\libnotify();
$notifier
    ->setSuccessImage($images . DIRECTORY_SEPARATOR . 'success.png')
    ->setFailureImage($images . DIRECTORY_SEPARATOR . 'failure.png');

$report = $script->addDefaultReport();
$report->addField($notifier, array(atoum\runner::runStop));

$extension = new mageekguy\atoum\autoloop\extension($script);
$extension
    ->setWatchedFiles(array(__DIR__))
    ->addToRunner($runner);
